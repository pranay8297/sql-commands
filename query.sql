
q1:
select * from cd.facilities

q2:
select name from cd.facilities

q3:
select * from cd.facilities where membercost > 0

q4:
select facid, name, membercost, monthlymaintenance from cd.facilities where membercost > 0 and membercost < monthlymaintenance/50

q5:
select * from cd.facilities where name like '%Tennis%'

q6
select * from cd.facilities where facid = '1' or facid = '5'
select * from cd.facilities where facid in (1 , 5)

q7
select name,
	CASE WHEN (monthlymaintenance > 100) then 
		'expensive'
	else 
		'cheap'
	end as cost
	from cd.facilities

reference : 'https://social.msdn.microsoft.com/Forums/sqlserver/en-US/29589970-d1b7-4e35-8746-81d578c77f17/created-a-new-column-in-a-select-statement-how-to-use-that-column-name-in-case-statement-in-the?forum=transactsql'

q8
select memid, surname, firstname, joindate from cd.members where joindate > '20120901'

q9
select distinct surname from cd.members order by surname limit 10;

q10
select surname from cd.members
UNION
select name from cd.facilities;

q11:
select firstname , surname , joindate from cd.members where joindate = (select max(joindate) from cd.members)

--------------------------------------------------------------------------------------------------
joins and subqueries

q1:
SELECT b.starttime from cd.members as m 
    inner join cd.bookings as b on m.memid = b.memid
    where m.firstname = 'David' and 
        m.surname = 'Farrell';

q2:
select b.starttime, f.name from cd.bookings as b 
	inner join cd.facilities as f on b.facid = f.facid 
        where f.name like 'Tennis%'
            and date(b.starttime) = '2012-09-21'
                order by b.starttime

q3:
 
select distinct m.firstname, m.surname from
	cd.members as m inner join cd.members as r on m.memid = r.recommendedby
	order by m.surname 

q4:

select m.firstname as memfname , m.surname as memsname , r.firstname as recfname , r.surname as recsname
from cd.members m left outer join cd.members r on r.memid = m.recommendedby
order by memsname ,memfname 

q5:
select distinct m.firstname as member , f.name as facility from 
cd.members m inner join cd.bookings b on m.memid = b.memid
inner join cd.facilities f on b.facid = f.facid 
where f.name like 'Tennis%'
order by m.firstname 

q6:
select m.firstname as member, f.name as facility,
case
	when m.memid = 0 then b.slots*f.guestcost
	else b.slots*f.membercost end as cost
from cd.members m inner join cd.bookings b on m.memid = b.memid
inner join cd.facilities f on b.facid = f.facid
where
b.starttime >= '2012-09-14' and
b.starttime < '2012-09-15' and
((m.memid = '0' and b.slots*f.guestcost > '30') or 
(m.memid != '0' and b.slots*f.membercost > 30))
order by cost desc

q7:

select distinct m.firstname as member , 
(select r.firstname as recommender from cd.members r where m.recommendedby = r.memid)
from cd.members m
order by m.firstname

q8


section -3

q1:
insert into cd.facilities values('9' , 'spa' , '20' , '30' , '100000' , '800')

q2:
insert into cd.facilities values('9' , 'spa' , '20' , '30' , '100000' , '800'),
('10' , 'Squash Court2' , '3.5' , '17.5' , '5000' , '80')

q6:
update cd.facilities f
set membercost = membercost + (select membercost*0.1 from cd.facilities where facid = 0),
	guestcost = guestcost + (select guestcost*0.1 from cd.facilities where facid = 0)
where facid = 1; 

q7:
delete  from cd.bookings

q8:
delete from cd.members where memid = 37

q9:
delete from cd.members where memid not in (select memid from cd.bookings)

-----------------------------------------------------------------------------------------------
AGGREGATES 

q9:
select facid, total from ( select facid, total, rank() over (order by total desc) rank from (
select facid, sum(slots) total
from cd.bookings
group by facid) as sumslots) as ranked where rank = 1

q10:
select firstname, surname, ((sum(bks.slots)+10)/20)*10 as hours, rank() over (order by ((sum(bks.slots)+10)/20)*10 desc) as rank
from cd.bookings bks inner join cd.members mems on bks.memid = mems.memid
group by mems.memid order by rank, surname, firstname;    

--------------------------------------
DATES

##For any given timestamp, work out the number of days remaining in the month. 
The current day should count as a whole day, regardless of the time. Use '2012-02-11 01:00:00' as an example timestamp for the purposes of making the answer. 
Format the output as a single interval value.##


select (date_trunc('month',ts.testts) + interval '1 month') - date_trunc('day', ts.testts) as remaining
from (select timestamp '2012-02-11 01:00:00' as testts) ts    


##''Return a count of bookings for each month, sorted by month''

select date_trunc('month' , starttime) as month, count(*) from cd.bookings group by month
order by month
	
	


